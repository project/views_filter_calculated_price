Views filter calculated price
===============

CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Author
  * Similar projects and how they are different

INTRODUCTION
------------
This module provides a commerce views filter by calculated price.

REQUIREMENTS
------------

Commerce payment

INSTALLATION
------------

1. Install module as usual via Drupal UI, Drush or Composer.
2. Go to "Extend" and enable the View filter calculated price.

CONFIGURATION
----------------


AUTHOR
------

shmel210
Drupal: (https://www.drupal.org/user/2600028)
Email: shmel210@zina.com.ua

Company: Zina Design
Website: (https://www.zina.design)
Drupal: (https://www.drupal.org/zina-design)
Email: info@zina.com.ua

SIMILAR PROJECTS AND HOW THEY ARE DIFFERENT
-------------------------------------------
There is no similar projects at this time.
